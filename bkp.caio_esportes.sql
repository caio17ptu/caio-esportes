-- MySQL dump 10.13  Distrib 8.0.13, for Linux (x86_64)
--
-- Host: localhost    Database: caio_esportes
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `contato` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `sexo` varchar(15) DEFAULT NULL,
  `descricao` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (5,'caio','caio17ptu@gmail.com','MASCULINO','Caio Francisco Elias'),(6,'caio','caio17ptu@gmail.com','MASCULINO','cccccccccccccccccccccccccccccccccccccccccccccccccccccccc'),(7,'caio','caio17ptu@gmail.com','FEMININO','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'),(8,'caio','caio17ptu@gmail.com','MASCULINO','aaaaaaaaaaa'),(9,'caio','caio17ptu@gmail.com','FEMININO','cccccccccccccccccccc'),(10,'caio','caio17ptu@gmail.com','MASCULINO','ccccccccccccc'),(11,'caio','caio17ptu@gmail.com','FEMININO','cccccccccccccccc'),(12,'caio','caio17ptu@gmail.com','MASCULINO','ccccccccccccc'),(13,'caio','caio17ptu@gmail.com','MASCULINO','cccccccccccccc'),(14,'caio','caio17ptu@gmail.com','FEMININO','cccccccccccc'),(15,'caio','caio17ptu@gmail.com','MASCULINO','aaaaaaaaaaaaaaaaaaa'),(16,'caio','caio17ptu@gmail.com','FEMININO','xxxxxxxxxxxxxxxxxxxxxxxxxxxxx'),(17,'caio','caio17ptu@gmail.com','MASCULINO','ccccccccccccccccccc'),(18,'caio','caio17ptu@gmail.com','FEMININO','cccccccccccccccc'),(19,'caio','caio17ptu@gmail.com','MASCULINO','vassssssssssssssssssssssss'),(20,'caio','caio17ptu@gmail.com','FEMININO','caio  s s  ssssss s s s s s s'),(21,'caio','caio17ptu@gmail.com','FEMININO','cccccccccccccccc'),(22,'caio','caio17ptu@gmail.com','FEMININO','ssssssssssssssssss'),(23,'','','FEMININO',''),(24,'caio','caio17ptu@gmail.com','FEMININO','ccccccccccc'),(25,'caio','caio17ptu@gmail.com','FEMININO','cccccccccccccc'),(26,'caio','caio17ptu@gmail.com','FEMININO','ccccccccccccccc'),(27,'caio','caio17ptu@gmail.com','FEMININO','ccccccc'),(28,'caio','caio17ptu@gmail.com','FEMININO','  cccccccccccccccc'),(29,'ccccccccccccccc','caio17ptu@gmail.com','FEMININO','ccccccccccc'),(30,'caio','caio17ptu@gmail.com','FEMININO','cccccccccccccc'),(31,'caio','caio17ptu@gmail.com','FEMININO','aaaaaaaaaaaaaaa');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticia_categoria`
--

DROP TABLE IF EXISTS `noticia_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `noticia_categoria` (
  `id_noticia` int(11) NOT NULL,
  `categoria` varchar(200) NOT NULL,
  PRIMARY KEY (`id_noticia`,`categoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticia_categoria`
--

LOCK TABLES `noticia_categoria` WRITE;
/*!40000 ALTER TABLE `noticia_categoria` DISABLE KEYS */;
INSERT INTO `noticia_categoria` VALUES (17,'basquete'),(17,'volei'),(18,'basquete'),(18,'volei'),(20,'basquete'),(20,'futebol'),(20,'volei'),(23,'futebol'),(24,'futebol'),(25,'futebol'),(26,'futebol'),(27,'futebol'),(28,'futebol'),(29,'futebol'),(30,'basquete'),(30,'futebol'),(31,'futebol'),(32,'volei'),(33,'futebol'),(34,'futebol'),(35,'volei'),(36,'futebol');
/*!40000 ALTER TABLE `noticia_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticias`
--

DROP TABLE IF EXISTS `noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `noticias` (
  `idnoticias` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `resenha` varchar(500) NOT NULL,
  `descricao` mediumtext NOT NULL,
  `dataInsercao` datetime DEFAULT NULL,
  `imagem` varchar(200) NOT NULL,
  UNIQUE KEY `idnoticias` (`idnoticias`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticias`
--

LOCK TABLES `noticias` WRITE;
/*!40000 ALTER TABLE `noticias` DISABLE KEYS */;
INSERT INTO `noticias` VALUES (23,'Gabigol e Everton Ribeiro confirmam presença no Jogo das Estrelas','Além do artilheiro e do dono do gol mais bonito do Brasileirão, evento organizado por Zico também contará com a participação de Petkovic, Mozer, Ronaldo Angelim e Júnior','verton Ribeiro, do Flamengo, e Gabigol, do Santos, vão participar pela primeira vez do Jogos das Estrelas. A dupla garantiu presença no evento organizado por Zico, que chegará à 15ª edição no próximo dia 27 de dezembro, no Maracanã.\r\n\r\nO evento tradicionalmente conta com a participação de jogadores renomados e ex-jogadores que fizeram história no futebol. Neste ano, a partida beneficente também contará com Júnior, Adílio, Uri Geller, Jayme de Almeida, Mozer, Carlos Alberto Santos, Aldair, Luizinho, Alcindo, Djalminha, Zinho, Carlos Germano, Fernando Santos, Magrão, Alex Dias, Sávio, Ronaldo Angelim, Petkovic, Léo Moura e Maicon.','2018-12-13 11:37:05','everton.jpg'),(24,'Tem vaga no seu time? Central analisa jogadores (e valores) de River Plate e Boca Juniors','As opções são boas, mas não muito baratas. Será que vale o investimento? Confira a lista','Será que algum jogador que disputou a final da Libertadores pode pintar no seu time na próxima temporada? Já pode esquecer qualquer possibilidade de contar com os millonarios Pity Martínez e Palacios, que estão com as malas prontas para Atlanta United e Real Madrid, respectivamente.\r\n\r\nAinda assim sobram muitos nomes. E o GloboEsporte.com selecionou alguns atletas do River Plate e do Boca Juniors que seriam reforços interessantes nas equipes brasileiras em 2019. Os valores de mercado e tempo de duração dos contratos dos atletas são do site Transfermakt. Veja os nomes e analise se eles têm vaga no seu clube.','2018-12-13 11:46:01','vaga.jpg'),(25,'Nikão desabafa após título do Atlético e revela luta contra alcoolismo: \"Me libertei disso\"','Na comemoração do título da Copa Sul-Americana, atacante celebra superação pessoal após quatro anos no Furacão. Camisa 11 lembra de Fernando Diniz, demitido em junho','\r\nNikão diz que previu título sofrido nos pênalti e lembra alcoolismo no início da carreira\r\n\r\n\r\nDepois de o Atlético-PR conquistar o título da Copa Sul-Americana, nesta quarta-feira, o atacante Nikão revelou que luta contra o vício em bebidas alcoólicas. Ainda no campo da Arena da Baixada, na comemoração da vitória nos pênaltis sobre o Junior Barranquilla, desabafou e exaltou o clube em que joga há quatro anos.','2018-12-13 11:48:26','nicao.jpeg'),(26,'Um gol em oito cobranças: Junior Barranquilla e a incrível sina dos pênaltis perdidos','Excluindo as disputas de penalidades, o time colombiano que perdeu a Sul-Americana para o Atlético-PR só acertou um pênalti em competições internacionais','Vice-campeão da Sul-Americana deste ano, Junior Barranquilla alcançou marca impressionante em competições internacionais em 2018: em oito pênaltis batidos excluindo as disputas de penalidades, o time colombiano acertou apenas um. Na final contra o Atlético-PR, Pérez chutou no travessão no jogo de ida, e Barrera cobrou para fora na partida de volta.\r\nNa Libertadores, foram quatro erros em quatro cobranças\r\n\r\nA sina do Junior nesta temporada começou logo na estreia no Grupo 8 da Libertadores deste ano. Na derrota para o Palmeiras por 3 a 0, em Barraquilla, Álvez chutou por cima do gol. A penalidade cobrada por Téo Gutiérrez na vitória por 1 a 0 sobre o Alianza Lima também passou sobre o travessão.\r\n\r\nNo empate com o Boca Juniors por 1 a 1, em casa, o goleiro argentino Andrada defendeu a cobrança de Ruiz. Para completar a sina na Libertadores, Fernando Prass pegou o pênalti batido por Barrera.\r\n\r\nMaldição continua na Sul-Americana\r\n\r\nA história parecia que ia mudar na Sul-Americana, quando logo de cara o Junior eliminou o Lanús na disputa de pênaltis, vencendo por 3 a 2: Barrera, Pérez e Viera acertaram, enquanto Téo Gutiérrez e Díaz perderam. Fora das disputas, a única cobrança correta veio no jogo de ida das quartas de final da Sul-Americana, quando Pérez marcou de pênalti na vitória sobre o Defensa y Justicia.\r\n\r\nMas a situação voltou a piorar no jogo de volta das semifinais da Sul-Americana, quando o pênalti batido por Sánchez foi defendido por Solis, goleiro do Independiente Santa Fe.','2018-12-13 11:51:01','sina.jpg'),(27,'Copa do Brasil 2019: sorteio acontece nesta quinta-feira; confira os potes','CaioEsportes.com transmite ao vivo a partir das 20h (de Brasília), direto da CBF','Nesta quinta-feira, às 20h (de Brasília), a Confederação Brasileira de Futebol realiza o sorteio dos primeiros duelos da Copa do Brasil de 2019. O GloboEsporte.com transmite ao vivo e acompanha em Tempo Real.\r\n\r\nA próxima edição do torneio será disputada nos moldes da de 2018: primeira fase com jogo único e mando de campo do clube pior ranqueado; segunda fase com jogo único e mando definido por sorteio.\r\n\r\nAo todo, 11 equipes entram diretamente nas oitavas de final: Palmeiras, Flamengo, Internacional, Grêmio, São Paulo e Atlético-MG (que terminaram o Brasileirão no G-6), Cruzeiro (atual bicampeão da Copa do Brasil), Atlético-PR (7º colocado no Brasileirão, entra de acordo com regulamento), Sampaio Corrêa (campeão da Copa do Nordeste), Paysandu (campeão da Copa Verde) e Fortaleza (campeão da Série B).\r\n\r\nNo sorteio desta quinta, outros 80 clubes estarão divididos em oito potes (dez em cada), de acordo com as pontuações do Ranking Nacional de Clubes. A CBF não divulgou os potes, mas é possível prever onde cada um será posicionado.','2018-12-13 11:53:51','copa.jpg'),(28,'A taça em imagens: veja como foi o jogo do Atlético-PR que deu o título da Copa Sul-Americana','Confira abaixo a história toda em fotos da final da Copa Sul-Americana entre Atlético-PR x Júnior Barranquilla, na Arena da Baixada','Além de Ramiro, o Grêmio tem encaminhada a saída de Bressan para o FC Dallas, dos Estados Unidos. A saída gera uma economia no clube, que teria obrigação de pagar cerca de R$ 30 milhões ao empresário Giuliano Bertolucci, que comprou 50% dos direitos de cada jogador em 2014, por US$ 4 milhões. Os negócios estão ligados. O clube hoje tem apenas 10% dos direitos de cada atleta.\r\n\r\nPai de Ramiro, Gilnei Benetti disse que o contrato com o Corinthians já está apalavrado, faltando apenas a assinatura de contrato. Ela pode acontecer até o fim da semana.','2018-12-13 15:44:52','atletico.jpg'),(29,'Santos diz ter acordo com Jorge Sampaoli, e técnico deve assinar contrato no fim de semana','Argentino viaja ao Brasil no sábado para definir detalhes finais de contrato para assumir equipe em 2019','Jorge Sampaoli deve ser mesmo o treinador do Santos em 2019. O Peixe anunciou que o ex-técnico da seleção argentina aceitou a proposta santista na tarde desta quinta-feira. Segundo o representante do argentino, Fernando Baredes, Sampaoli viaja ao Brasil no sábado para resolver detalhes finais e assinar contrato, a princípio válido por dois anos.\r\n\r\nSe tudo caminhar dentro dos conformes, Sampaoli substituirá Cuca, que deixou o comando do Santos após o Campeonato Brasileiro para tratar um problema cardíaco.\r\n\r\nEm comunicado publicado no site do Santos, o presidente José Carlos Peres afirma que a negociação é uma \"ousadia\" do clube e que ela impactará no futebol brasileiro.','2018-12-13 16:16:44','gettyimages-987859174 (1).jpg'),(30,'Presidente do Grêmio confirma negócio encaminhado de Ramiro com o Corinthians','Romildo Bolzan diz que negócio fará o clube economizar: \"Estamos amarrados\"','O presidente do Grêmio, Romildo Bolzan, confirmou que a negociação de Ramiro com o Corinthians está \"praticamente encaminhada\". Faltam detalhes para o acerto do contrato, que deve ser válido por quatro temporadas.\r\n\r\n– Estamos amarrados nesse negócio – disse o dirigente, ao site \"GaúchaZH\".\r\n\r\n– Eliminamos um risco enorme de ter que pagar US$ 8 milhões (R$ 30,8 milhões, ao empresário Giuliano Bertolucci). Está praticamente encaminhado o negócio do Ramiro. Faz parte da disposição do jogador, que quer dar um passo à frente. É mais do que legítimo isso – disse o dirigente.\r\n\r\nAinda não está claro se o Corinthians desenbolsará algum valor num primeiro momento.','2018-12-13 16:19:34','img_6212_Imfk9TS.jpg'),(35,'Medina estreia bem em Pipeline e avança direto ao round 3; Filipinho vai para repescagem','Líder do ranking e favorito ao título, paulista derrota Connor O\'Leary e Benji Brand, ganhando mais tempo de descanso até a próxima fase. Julian Wilson também vence no round 1','tual líder do ranking e favorito ao bicampeonato mundial, Gabriel Medina estreou bem no Pipe Masters, a última etapa do Circuito. Nesta quinta, o camiseta amarela derrotou o australiano Connor O\'Leary e o havaiano Benji Brand, avançando direto ao round 3. Com 56,190 pontos na temporada, o paulista de Maresias só pode ser ultrapassado por Julian Wilson e Filipe Toledo. Desses apenas o australiano venceu no round 1. Derrotado por Matt Wilkinson, da Austrália, Filipinho terá de vencer o havaiano Benji Brand na repescagem (round 2) para seguir com chances. O round 1 segue em andamento com quatro brasileiros - Ian Gouveia, Jesse Mendes, Willian Cardoso e Michael Rodrigues - ainda a competir.','2018-12-13 19:58:40','medina2.jpg'),(36,'Com telefonema de Felipão, Raphael Veiga volta ao Palmeiras com novo status','Meia emprestado ao Atlético-PR teve seu melhor ano com protagonismo no título da Sul-Americana. Palmeiras não abre mão de sua volta, e ele conta que recebeu ligação de Felipão','Emprestado ao Atlético-PR depois de ter poucas oportunidades em 2017, o meia Raphael Veiga agora já chama atenção do diretoria do Palmeiras. Seu retorno é dado como certo e ainda contou com um empurrão a mais do técnico Felipão.\r\n\r\nSegundo a repórter Nadja Mauad, da RPC, o treinador do Palmeiras ligou para Raphael Veiga para parabenizar pela temporada, além de reafirmar que existe lugar para ele no elenco do campeão brasileiro. Antes disso, o diretor de futebol Alexandre Mattos já havia afirmado que não existe cláusula de renovação de empréstimo e contratação.','2018-12-13 20:01:14','athletico-x-junior-barraquilla-ar-42-.jpg');
/*!40000 ALTER TABLE `noticias` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-14 11:54:18
