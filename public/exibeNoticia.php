<html>
    <?php
    include "head.html";
    require_once $_SERVER['DOCUMENT_ROOT'] . "/DAO/noticiaDao.php";
   
    
    $id =  $_GET['id'];
    $ndao = new noticiaDao();
    $not = $ndao->buscarNoticia($id);
    ?>
    <body>
        <?php
        include "header.php"
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="exibeNoticia">
                        <div class="col-md-12">
                        <h1>
                            <?php echo $not[0]['titulo']?>
                        </h1>
                        </div>
                        <div class="col-md-12" id="imagem">
                            <img src="../imagens/<?php echo $not[0]['imagem']?>" alt="" class="img img-thumbnail"/>
                        </div>
                        <div class="col-md-12">
                        <h2>
                            <?php echo $not[0]['resenha']?>
                        </h2>
                        </div>
                        <div class="col-md-12">
                        <p>
                            <?php echo $not[0]['descricao']?>
                        </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include "footer.html"
        ?>
        
    </body>

</html>



