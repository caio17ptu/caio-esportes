<?php
ini_set('display_errors', true);

require_once $_SERVER['DOCUMENT_ROOT'] . "/DAO/noticiaDao.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/Entidades/Noticia.php";


switch ($_REQUEST['acao']) {
    case 'salvarNoticia':
        $mensagem = '';
        if (!preg_match('/^image\/(pjpeg|jpeg|png|gif|bmp)$/',$_FILES['arquivo']['type'])) {
            $mensagem = 'Arquivo selecionado não é imagem';
            header("Location: ../adicionaNoticia.php?erro=" . $mensagem);
        }else
        if (empty($_REQUEST['categorias'])) {
            $mensagem = 'Selecione ao menos uma categoria';
            header("Location: ../adicionaNoticia.php?erro=" . $mensagem);
        } else
        if (empty($_FILES['arquivo']["name"])) {
            $mensagem = 'Selecione uma imagem';
            header("Location: ../adicionaNoticia.php?erro=" . $mensagem);
        } else
        if (empty($_REQUEST['titulo'])) {
            $mensagem = 'Insira um titulo';
            header("Location: ../adicionaNoticia.php?erro=" . $mensagem);
        } else
        if (empty($_REQUEST['descricao'])) {
            $mensagem = 'Insira uma Descrição';
            header("Location: ../adicionaNoticia.php?erro=" . $mensagem);
        } else
        if (empty($_REQUEST['resenha'])) {
            $mensagem = 'SInsira uma resenha';
            header("Location: ../adicionaNoticia.php?erro=" . $mensagem);
        } else {


            $categorias = $_REQUEST['categorias'];
            $noticia = new Noticia();
            $noticia->setImagem($_FILES['arquivo']["name"]);
            $temp = $_FILES["arquivo"]["tmp_name"];
            $noticia->setTitulo($_REQUEST['titulo']);
            $noticia->setDescricao($_REQUEST['descricao']);
            $noticia->setResenha($_REQUEST['resenha']);

            $dao = new noticiaDao();
            if (move_uploaded_file($temp, '../imagens/' . $noticia->getImagem())) {
                if (!$dao->inserirNoticia($noticia, $categorias)) {
                    $mensagem = 'Cadastro bem sucedido';
                    header("Location: ../adicionaNoticia.php?erro=" . $mensagem);
                } else {
                    $mensagem = 'Erro ao inserir Noticia';
                    header("Location: ../adicionaNoticia.php?erro=" . $mensagem);
                }
            }
        }
        break;
    default:
        echo 'Ação não identificada';
        break;
}
?>>
