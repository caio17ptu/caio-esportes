<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/Entidades/Noticia.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/Util/Conexao.php";

class noticiaDao {

    public function inserirNoticia($noticia, $categoria) {


        try {
            $sql = 'insert into noticias (titulo, resenha, descricao, dataInsercao, imagem) values (:titulo, :resenha, :descricao, :dataInsercao, :imagem);';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':titulo', $noticia->getTitulo());
            $p_sql->bindValue(':resenha', $noticia->getResenha());
            $p_sql->bindValue(':descricao', $noticia->getDescricao());
            $p_sql->bindValue(':dataInsercao', date("Y-m-d H:i:s"));
            $p_sql->bindValue(':imagem', $noticia->getImagem());
            if ($p_sql->execute()) {
                $id = Conexao::getInstance()->lastInsertId();
                foreach ($categoria as $c) {
                    try{
                        $sql = 'insert into noticia_categoria (id_noticia, categoria) values (:id, :cat);';
                        $p_sql = Conexao::getInstance()->prepare($sql);
                        $p_sql->bindValue(':id', $id);
                        $p_sql->bindValue(':cat', $c);
                        $p_sql->execute();
                    } catch (Exception $ex) {
                         print 'Erro no banco de dados: ' . $ex;
                    }
                }
            }
            return false;
        } catch (Exception $exc) {
            print 'Erro no banco de dados: ' . $ex;
        }
    }

    public function listarPrincipais() {
        try {
            $sql = "SELECT * FROM caio_esportes.noticias order by dataInsercao desc limit 5;";

            $p_sql = Conexao::getInstance()->prepare($sql);
            if ($p_sql->execute()) {
                $noticias = $p_sql->fetchAll();
                return $noticias;
            }
            return false;
        } catch (Exception $exc) {
            print 'Erro no banco de dados: ' . $exc;
        }
    }
    
    public function buscarNoticia($id) {
        try {
            $sql = "SELECT * FROM caio_esportes.noticias where idNoticias = :id";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            if ($p_sql->execute()) {
                $noticia = $p_sql->fetchAll(PDO::FETCH_ASSOC);
                return $noticia;
            }
            return false;
        } catch (Exception $exc) {
            print 'Erro no banco de dados: ' . $exc;
        }
    }
    
    public function listarCategoria($categ) {
        try {
            $sql = "select distinct (id_noticia), titulo,descricao, imagem , resenha, dataInsercao from noticias join noticia_categoria on id_noticia = idnoticias where categoria = :categ order by (dataInsercao) desc;";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':categ', $categ);
            if ($p_sql->execute()) {
                $noticias = $p_sql->fetchAll();
                return $noticias;
            }
            return false;
        } catch (Exception $exc) {
            print 'Erro no banco de dados: ' . $exc;
        }
    }
     
}

?>
