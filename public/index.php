<html>
    <?php
    include "head.html";
    require_once $_SERVER['DOCUMENT_ROOT'] . "/DAO/noticiaDao.php";
    $ndao = new noticiaDao();
    $noticias = $ndao->listarPrincipais();
    ?>
    <body>
         
        <?php
        include "header.php"
        ?>
        <?php
        if (!empty($_GET['erro'])) {
            echo "<script>
            alert('" . $_GET['erro'] . "');
            </script>";
        }
        ?>
        <?php
            if(empty($noticias)){ ?>
        <div class="container"> 
            <div style="text-align: center">
                <script>
                alert('Não temos Noticias no memento\nVisite-nos mais tarde');
                </script>"
                <img src="imagens/imagem defalt.jpg" alt="" class="img img-responsive"/>  
            </div>
        </div>
            <?php }else { ?>
       
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-xs-12 col-sm-12 noticias box" id="noticia_principal" style="background-image: url('imagens/<?php echo $noticias[0]["imagem"]; ?>');" >
                    <a href="exibeNoticia.php?id=<?php echo $noticias[0]['idnoticias']; ?>">
                        <h1>
                            <?php echo $noticias[0]["titulo"]; ?>
                        </h1>
                    </a>

                </div>
                <div class="col-md-5 col-xs-12 col-sm-12">
                    <div class="col-md-12 col-xs-12 col-sm-6 noticia_sec noticias box" style="background-image: url('imagens/<?php echo $noticias[1]["imagem"]; ?>')">
                        <a href="exibeNoticia.php?id=<?php echo $noticias[1]['idnoticias']; ?>">
                        <h2>
                            <?php echo $noticias[1]["titulo"]; ?>
                        </h2>
                    </a>
                    </div>
                    <div class="col-md-12  col-xs-12 col-sm-6 noticia_sec noticias box" style="background-image: url('imagens/<?php echo $noticias[2]["imagem"]; ?>')">
                       <a href="exibeNoticia.php?id=<?php echo $noticias[2]['idnoticias']; ?>">
                        <h2>
                            <?php echo $noticias[2]["titulo"]; ?>
                        </h2>
                        </a>
                    </div>
                </div>

            </div>
           
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12 noticias">
                    <div class="col-md-5 col-xs-12 col-sm-12">
                        <img src="imagens/<?php echo $noticias[3]["imagem"]; ?>" alt="" class="img img-responsive ntc"/>
                    </div>
                    <div class="col-md-7 col-xs-12 col-sm-12">
                        <a href="exibeNoticia.php?id=<?php echo $noticias[3]['idnoticias']; ?>">
                        <h1>
                            <?php echo $noticias[3]["titulo"]; ?>
                        </h1>
                        </a>
                        <h3>
                            <?php echo $noticias[3]["resenha"]; ?>
                        </h3>
                    </div>
                </div>
            </div>
         
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12 bord noticias">
                    <div class="col-md-5 col-xs-12 col-sm-12">
                        <img src="imagens/<?php echo $noticias[4]["imagem"]; ?>" alt="" class="img img-responsive ntc"/>
                    </div>
                    <div class="col-md-7 col-xs-12 col-sm-12">
                        <a href="exibeNoticia.php?id=<?php echo $noticias[4]['idnoticias']; ?>">
                        <h1>
                            <?php echo $noticias[4]["titulo"]; ?>
                        </h1>
                        </a>
                        <h3>
                            <?php echo $noticias[4]["resenha"]; ?>
                        </h3>

                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php
        include "footer.html";
        ?>

            
    </body>
</html>
