<html>
    <?php
    include "head.html";
    include './DAO/noticiaDao.php';
    $id = $_GET['id'];
    $noticias = noticiaDao::listarCategoria($id);
    ?>
    <body>
        
        <?php
        include "header.php"
        ?>
        
        <?php 
            if(empty($noticias)){
               $mensagem = 'Não temos Noticia para esta categoria';
               header("Location: index.php?erro=".$mensagem);
            }
                
        ?>
        <div class="container">
            <div class="row noticias">
                <div class="col-lg-11 col-lg-offset-1">
                    <h1 class="col-md-12" style="text-align: center;">
                        Noticias de <?php echo $id; ?>
                    </h1>
                    
                <?php foreach ($noticias as $row) { ?>
                <div class="col-md-12 categoria"  style="content: initial">
                    <div class="col-md-6 box">
                        <img src="../imagens/<?php echo $row['imagem'];?>" alt="" class="img-responsive" >
                    </div>
                    <div class="col-md-6">
                        <a href="exibeNoticia.php?id=<?php echo $row['id_noticia']; ?>"><h1><?php echo $row['titulo'];?> </h1></a>
                        <h3><?php echo $row['resenha'];?> </h3>
                    </div>
                           

                    </div>
                <?php } ?>
                </div>
        </div>   
        </div>
        <?php
        include "footer.html";
        ?>
    </body>


</html>

