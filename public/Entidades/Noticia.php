<?php

class Noticia {
    
    private $id;
    private $titulo;
    private $resenha;
    private $descricao;
    private $imagem;
    private $dataInsercao;
    private $dataAlteracao;
    
    function __construct() {
        
    }
    
    function getid() {
        return $this->id;
    }
    function getTitulo() {
        return $this->titulo;
    }
    function getResenha(){
        return $this->resenha;
    }
    function getDescricao(){
        return $this->descricao;
    }
    function getImagem(){
        return $this->imagem;
    }
    function getDatIns(){
        return $this->dataInsercao;
    }
    function getDatAlt(){
        return $this->dataAlteracao;
    }
    function setId($id) {
        $this->id = $id;
    }
    function setTitulo($titulo){
        $this->titulo = $titulo;
    }
    function setResenha($resenha){
        $this->resenha = $resenha;
    }
    function setDescricao($descricao){
        $this->descricao = $descricao;
    }
    function setImagem($imagem){
        $this->imagem = $imagem;
    }
    function setDataInser($data){
        $this->dataInsercao = $data;
    }
    function setDataAlt($data){
        $this->dataAlteracao = $data;
    }
}
